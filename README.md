## Responsive Case Study

A case study to comprehend Responsive Web Design Fundamentals used by only CSS and HTML.

Here are some screenshots in different width sizes.

**1125px**

https://i.hizliresim.com/grvJY3.jpg


**940px**

![Alt text](https://i.hizliresim.com/P12Za9.jpg)



**700px**

![Alt text](https://i.hizliresim.com/7aWr15.jpg)



**400px**

![Alt text](https://i.hizliresim.com/Ll8WRj.jpg)